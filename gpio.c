#include "gpio.h"

int exportPin(int pin) {
	char buffer[3];
	ssize_t bytes;
	int file;
	file = open("/sys/class/gpio/export", O_WRONLY);
	if (file == -1) return -1;
	bytes= snprintf(buffer, 3, "%d", pin);
	if (write(file, buffer, bytes)==-1) return -2;
	close(file);
	return 0;
}
 
int unexportPin(int pin) {
	char buffer[3];
	ssize_t bytes;
	int file;
	file = open("/sys/class/gpio/unexport", O_WRONLY);
	if (file == -1) return -1;
	bytes = snprintf(buffer, 3, "%d", pin);
	if (write(file, buffer, bytes)==-1)return -2;
	close(file);
	return 0;
}
 
int directionPin(int pin, int mode) {	
	char path[35];
	int file;
	snprintf(path, 35, "/sys/class/gpio/gpio%d/direction", pin);
	file = open(path, O_WRONLY);
	if (file == -1)	return -1;
	if (write(file, mode==IN?"in":"out", mode==IN?2:3)==-1) return -2;
	close(file);
	return 0;
}
 
int readPin(int pin) {
	char path[30];
	char value[3];
	int file;
	snprintf(path, 30, "/sys/class/gpio/gpio%d/value", pin);
	file = open(path, O_RDONLY);
	if (file == -1) return -1;
	if (read(file, value, 3) == -1) return -2;
	close(file);
	return(atoi(value));
}
 
int writePin(int pin, int value) {
	char path[30];
	int file;
	snprintf(path, 30, "/sys/class/gpio/gpio%d/value", pin);
	file = open(path, O_WRONLY);
	if (file == -1) return -1;
	if (write(file, value==HIGH?"1":"0", 1) == -1) return -2;
	close(file);
	return(0);
}
 
