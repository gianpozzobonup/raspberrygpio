#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
 
#define IN  0
#define OUT 1
 
#define LOW  0
#define HIGH 1

int exportPin(int pin);
int unexportPin(int pin);
int directionPin(int pin, int dir);
int readPin(int pin);
int writePin(int pin, int value);
